import 'whatwg-fetch';
import checkStatus from '../helpers/checkStatus';
import parseJSON from '../helpers/parseJSON';

export const GET_OBJECT_LIST_START = 'GET_OBJECT_LIST_START';
export const GET_OBJECT_LIST_SUCCESS = 'GET_OBJECT_LIST_SUCCESS';
export const GET_OBJECT_LIST_FAIL = 'GET_OBJECT_LIST_FAIL';

const getObjectListStart = () => ({
  type: GET_OBJECT_LIST_START,
});

const getObjectListSuccess = data => ({
  type: GET_OBJECT_LIST_SUCCESS,
  data,
});

const getObjectListFail = error => ({
  type: GET_OBJECT_LIST_FAIL,
  error,
});

export const getObjectList = () => {
  return (dispatch) => {
    dispatch(getObjectListStart());
    fetch('https://api.myjson.com/bins/10aoxx')
      .then(checkStatus)
      .then(parseJSON)
      .then(data => dispatch(getObjectListSuccess(data.objects)))
      .catch(error => dispatch(getObjectListFail(error)));
  };
};
