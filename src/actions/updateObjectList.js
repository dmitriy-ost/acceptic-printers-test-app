import 'whatwg-fetch';
import checkStatus from '../helpers/checkStatus';
import parseJSON from '../helpers/parseJSON';

export const UPDATE_OBJECT_LIST_START = 'UPDATE_OBJECT_LIST_START';
export const UPDATE_OBJECT_LIST_SUCCESS = 'UPDATE_OBJECT_LIST_SUCCESS';
export const UPDATE_OBJECT_LIST_FAIL = 'UPDATE_OBJECT_LIST_FAIL';

const updateObjectListStart = () => ({
  type: UPDATE_OBJECT_LIST_START,
});

const updateObjectListSuccess = data => ({
  type: UPDATE_OBJECT_LIST_SUCCESS,
  data,
});

const updateObjectListFail = error => ({
  type: UPDATE_OBJECT_LIST_FAIL,
  error,
});

export const updateObjectList = formValues => {
  const options = {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(formValues),
  };
  return (dispatch) => {
    dispatch(updateObjectListStart());
    fetch('https://api.myjson.com/bins/10aoxx', options)
      .then(checkStatus)
      .then(parseJSON)
      .then(data => dispatch(updateObjectListSuccess(data.objects)))
      .catch(error => dispatch(updateObjectListFail(error)));
  };
};
