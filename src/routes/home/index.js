import React from 'react';
import Layout from '../../components/Layout';
import ObjectList from '../../components/ObjectList';

const HomePage = () => (
  <Layout>
    <ObjectList />
  </Layout>
);

export default HomePage;
