export const greaterThan = num => (value, previousValue) => {
  const parsed = parseInt(value, 10);
  return value.length ? parsed > num ? parsed : previousValue : '';
};

export const dimensions = (value, previousValue) => {
  const onlyNums = value.replace(/[^\d]/g, '');
  switch (onlyNums.length) {
    case 1:
    case 2:
      return parseInt(onlyNums, 10) ? onlyNums : previousValue;
    case 3:
    case 4:
      return parseInt(onlyNums, 10) ? `${onlyNums.slice(0, 2)}x${onlyNums.slice(2)}` : previousValue;
    case 5:
    case 6:
      return parseInt(onlyNums, 10) ? `${onlyNums.slice(0, 2)}x${onlyNums.slice(2, 4)}x${onlyNums.slice(4)}` : previousValue;
    default:
      return previousValue;
  }
};

export const boolean = value => !!value;
