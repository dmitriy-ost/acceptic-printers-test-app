import React from 'react';
import { connect } from 'react-redux';
import { Alert } from 'react-bootstrap';

const StatusBar = ({ loading, saving, error, saved }) => (
  <Alert
    bsStyle={error ? 'danger' : saving ? 'warning' : saved ? 'success' : 'info'}
  >
    {error ? error.message : loading ? 'Loading...' : saving ? 'Saving objects...' : saved ? 'Successfully saved' : 'Nothing happened'}
  </Alert>
);

const mapStateToProps = ({ objectList: { error, loading, saving, saved } }) => ({
  error,
  loading,
  saving,
  saved,
});

export default connect(mapStateToProps, null)(StatusBar);
