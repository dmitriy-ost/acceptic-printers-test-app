import React, { PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { reduxForm, FieldArray } from 'redux-form';

import RenderObjects from './RenderObjects';

import { getObjectList } from '../../actions/getObjectList';
import { updateObjectList } from '../../actions/updateObjectList';

const mapStateToProps = ({ objectList }) => ({
  objectList,
  initialValues: {
    objects: objectList.data,
  },
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    getObjectList,
    updateObjectList,
  }, dispatch),
});

class ObjectList extends React.Component {

  static propTypes = {
    actions: PropTypes.shape({
      getObjectList: PropTypes.func,
      updateObjectList: PropTypes.func,
    }),
  };

  constructor(props) {
    super(props);

    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
    this.props.actions.getObjectList();
  }

  onSubmit(formValues) {
    this.props.actions.updateObjectList(formValues);
  }

  render() {
    const { change, handleSubmit } = this.props;

    return (
      <form onSubmit={handleSubmit(this.onSubmit)}>
        <FieldArray
          name="objects"
          component={RenderObjects}
          change={change}
        />
      </form>
    );
  }
}

ObjectList = reduxForm({
  form: 'objectList',
  enableReinitialize: true,
})(ObjectList);

export default connect(mapStateToProps, mapDispatchToProps)(ObjectList);
