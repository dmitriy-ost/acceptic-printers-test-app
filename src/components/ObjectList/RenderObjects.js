import React from 'react';
import { Row, Col, Panel, ButtonGroup, Button } from 'react-bootstrap';
import ObjectItem from '../ObjectItem';
import StatusBar from '../StatusBar';

const RenderObjects = ({ fields, change }) => {

  const createId = (id) => {
    if (fields.getAll().find(field => field.id === id)) {
      return createId(id + 1);
    }

    return id;
  };

  return (
    <div>
      <Panel>
        <Row>
          <Col md={6}>
            <ButtonGroup>
              <Button
                bsStyle="primary"
                bsSize="large"
                type="button"
                onClick={() => fields.unshift({ id: createId(fields.length) })}
              >
                <i className="glyphicon glyphicon-plus" />
                &nbsp;
                Add New Object
              </Button>
              <Button
                bsStyle="success"
                bsSize="large"
                type="submit"
              >
                <i className="glyphicon glyphicon-floppy-disk" />
                &nbsp;
                Save Objects
              </Button>
            </ButtonGroup>
          </Col>
          <Col md={6}>
            <StatusBar />
          </Col>
        </Row>
      </Panel>
      {fields.map((fieldName, index) => {
        const field = fields.get(index);
        return (
          <ObjectItem
            key={field.id}
            field={field}
            fieldName={fieldName}
            change={change}
            removeObject={() => fields.remove(index)}
          />
        );
      })}
    </div>
  );
};

export default RenderObjects;
