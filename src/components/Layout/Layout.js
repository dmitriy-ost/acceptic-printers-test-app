import React from 'react';
import { Grid } from 'react-bootstrap';
import s from './Layout.css';

const Layout = props => (
  <Grid {...props} />
);

export default Layout;
