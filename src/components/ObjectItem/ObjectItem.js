import React, { Component, PropTypes } from 'react';
import { Field } from 'redux-form';
import { Panel, Row, Col, Image, Accordion, Button } from 'react-bootstrap';
import * as fieldsNormalizers from '../../helpers/fieldsNormalizers';
import RenderField from './RenderField';

class ObjectItem extends Component {

  static propTypes = {
    field: PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
      pictureURL: PropTypes.string,
      dimensions: PropTypes.string,
      units: PropTypes.string,
      printer: PropTypes.string,
      material: PropTypes.string,
      quantity: PropTypes.number,
      printerMode: PropTypes.string,
      layerThickness: PropTypes.string,
      color: PropTypes.string,
      active: PropTypes.bool,
      comments: PropTypes.string,
    }),
    fieldName: PropTypes.string,
    change: PropTypes.func,
    removeObject: PropTypes.func,
  };

  constructor(props) {
    super(props);

    this.state = {
      units: ['mm', 'cm', 'm', 'inch', 'feet'],
      printers: ['A', 'B', 'C'],
      availableMaterials: {
        A: ['A1', 'A2', 'A3', 'A4', 'A5'],
        B: ['B1', 'B2', 'B3', 'B4'],
        C: ['C1', 'C2', 'C3'],
      },
      materials: [],
      availableColors: {
        A: ['A-Red', 'A-Green', 'A-Blue'],
        C: ['C-Cyan', 'C-Magenta', 'C-Yellow', 'C-Blank', 'C-White'],
      },
      colors: [],
      availablePrinterModes: {
        A: ['AA1', 'AA2', 'AA3'],
      },
      printerModes: [],
      availableLayerThicknesses: {
        B: ['BB1', 'BB2', 'BB3', 'BB4'],
      },
      layerThicknesses: [],
      showAdvancedFields: false,
    };

    this.setDependentFields = this.setDependentFields.bind(this);
  }

  componentDidMount() {
    this.setDependentFields(this.props.field.printer);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.field.printer !== nextProps.field.printer) {
      this.setDependentFields(nextProps.field.printer);
    }
  }

  setDependentFields(printer) {
    this.setState({
      materials: this.state.availableMaterials[printer] || [],
      printerModes: this.state.availablePrinterModes[printer] || [],
      layerThicknesses: this.state.availableLayerThicknesses[printer] || [],
      colors: this.state.availableColors[printer] || [],
    });

    if (!this.state.availablePrinterModes[printer]) {
      this.props.change(`${this.props.fieldName}.printerMode`, null);
    }

    if (!this.state.availableLayerThicknesses[printer]) {
      this.props.change(`${this.props.fieldName}.layerThickness`, null);
    }

    if (!this.state.availableColors[printer]) {
      this.props.change(`${this.props.fieldName}.color`, null);
    }
  }

  render() {
    const { field, fieldName, removeObject } = this.props;
    const {
      units,
      printers,
      materials,
      printerModes,
      layerThicknesses,
      colors,
      showAdvancedFields,
    } = this.state;

    return (
      <Panel header={`# ${field.id}`}>
        <Row>
          <Col md={3}>
            <Image src={field.pictureURL} responsive />
            <Field
              id={`${fieldName}.pictureURL`}
              name={`${fieldName}.pictureURL`}
              type="text"
              component={RenderField}
              label="Image URL"
            />
          </Col>
          <Col md={3}>
            <Field
              id={`${fieldName}.name`}
              name={`${fieldName}.name`}
              type="text"
              component={RenderField}
              label="File Name"
            />
            <Row>
              <Col md={7}>
                <Field
                  id={`${fieldName}.dimensions`}
                  name={`${fieldName}.dimensions`}
                  type="text"
                  component={RenderField}
                  label="Dimensions"
                  normalize={fieldsNormalizers.dimensions}
                />
              </Col>
              <Col md={5}>
                <div className="form-group">
                  <label htmlFor={`${fieldName}.units`}>&zwnj;</label>
                  <Field
                    id={`${fieldName}.units`}
                    name={`${fieldName}.units`}
                    type="text"
                    component="select"
                    className="form-control"
                  >
                    <option />
                    {units.map(unit => (
                      <option key={unit} value={unit}>{unit}</option>
                    ))}
                  </Field>
                </div>
              </Col>
            </Row>
            <Field
              id={`${fieldName}.comments`}
              name={`${fieldName}.comments`}
              type="text"
              component={RenderField}
              componentClass="textarea"
              label="Comments"
            />
          </Col>
          <Col md={3}>
            <div className="form-group">
              <label htmlFor={`${fieldName}.printer`}>Printer</label>
              <Field
                id={`${fieldName}.printer`}
                name={`${fieldName}.printer`}
                type="text"
                component="select"
                className="form-control"
              >
                <option />
                {printers.map(printer => (
                  <option key={printer} value={printer}>{printer}</option>
                ))}
              </Field>
            </div>
            {materials.length ? <div className="form-group">
              <label htmlFor={`${fieldName}.material`}>Material</label>
              <Field
                id={`${fieldName}.material`}
                name={`${fieldName}.material`}
                type="text"
                component="select"
                className="form-control"
              >
                <option />
                {materials.map(material => (
                  <option key={material} value={material}>{material}</option>
                ))}
              </Field>
            </div> : null}
            <Accordion>
              <Panel
                expanded={showAdvancedFields}
                header={showAdvancedFields ? 'Hide advanced fields' : 'Show advanced fields'}
                onEnter={() => this.setState({ showAdvancedFields: true })}
                onExit={() => this.setState({ showAdvancedFields: false })}
                eventKey="1"
              >
                {printerModes.length ? <div className="form-group">
                  <label htmlFor={`${fieldName}.printerMode`}>Printer Mode</label>
                  <Field
                    id={`${fieldName}.printerMode`}
                    name={`${fieldName}.printerMode`}
                    type="text"
                    component="select"
                    className="form-control"
                  >
                    <option />
                    {printerModes.map(mode => (
                      <option key={mode} value={mode}>{mode}</option>
                    ))}
                  </Field>
                </div> : null}
                {layerThicknesses.length ? <div className="form-group">
                  <label htmlFor={`${fieldName}.layerThickness`}>Layer Thickness</label>
                  <Field
                    id={`${fieldName}.layerThickness`}
                    name={`${fieldName}.layerThickness`}
                    type="text"
                    component="select"
                    className="form-control"
                  >
                    <option />
                    {layerThicknesses.map(thickness => (
                      <option key={thickness} value={thickness}>{thickness}</option>
                    ))}
                  </Field>
                </div> : null}
                {colors.length ? <div className="form-group">
                  <label htmlFor={`${fieldName}.color`}>Color</label>
                  <Field
                    id={`${fieldName}.color`}
                    name={`${fieldName}.color`}
                    type="text"
                    component="select"
                    className="form-control"
                  >
                    <option />
                    {colors.map(color => (
                      <option key={color} value={color}>{color}</option>
                    ))}
                  </Field>
                </div> : null}
              </Panel>
            </Accordion>
          </Col>
          <Col md={3}>
            <Field
              id={`${fieldName}.quantity`}
              name={`${fieldName}.quantity`}
              type="number"
              component={RenderField}
              label="Quantity"
              normalize={fieldsNormalizers.greaterThan(0)}
            />
            <Field
              id={`${fieldName}.active`}
              name={`${fieldName}.active`}
              type="checkbox"
              component={RenderField}
              label="Active"
              normalize={fieldsNormalizers.boolean}
            />
            <Button
              bsStyle="warning"
              onClick={removeObject}
            >
              <i className="glyphicon glyphicon-trash" />
              Remove Object
            </Button>
          </Col>
        </Row>
      </Panel>
    );
  }
}

export default ObjectItem;
