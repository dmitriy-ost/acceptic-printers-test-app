import React from 'react';
import { FormGroup, ControlLabel, FormControl, Checkbox } from 'react-bootstrap';

const RenderField = ({ input, label, id, componentClass, type }) => (
  type === 'checkbox' ?
    <Checkbox {...input}>
      {label}
    </Checkbox> :
    <FormGroup controlId={id}>
      <ControlLabel>{label}</ControlLabel>
      <FormControl {...input} componentClass={componentClass} />
    </FormGroup>
);

export default RenderField;
