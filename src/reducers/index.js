import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import objectList from './objectList';

const rootReducer = combineReducers({
  objectList,
  form: formReducer,
});

export default rootReducer;
