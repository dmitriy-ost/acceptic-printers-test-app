import * as getActions from '../actions/getObjectList';
import * as updateActions from '../actions/updateObjectList';

const initialState = {
  data: [],
  error: null,
  loading: false,
  saving: false,
  saved: false,
};
const objectList = (state = initialState, { type, data, error }) => {
  switch (type) {
    case getActions.GET_OBJECT_LIST_START:
      return {
        ...state,
        loading: true,
      };
    case updateActions.UPDATE_OBJECT_LIST_START:
      return {
        ...state,
        saving: true,
      };
    case getActions.GET_OBJECT_LIST_SUCCESS:
      return {
        ...initialState,
        data,
      };
    case updateActions.UPDATE_OBJECT_LIST_SUCCESS:
      return {
        ...initialState,
        data,
        saved: true,
      };
    case getActions.GET_OBJECT_LIST_FAIL:
    case updateActions.UPDATE_OBJECT_LIST_FAIL:
      return {
        ...initialState,
        error,
      };
    default:
      return state;
  }
};

export default objectList;
